import cocotb
import random

from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, ReadOnly
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure
from cocotb.binary import BinaryValue

from cocotb.utils import get_sim_time

CLK_PERIOD_A = 2
CLK_PERIOD_B = 4


@cocotb.test()
def simple_test(dut):
    """
        Simple test: it writes random values to all memory addresses and reads them back.
    """

    dut._log.info('Init Clocks')
    cocotb.fork(Clock(dut.CLKA, CLK_PERIOD_A, units='ns').start())
    cocotb.fork(Clock(dut.CLKB, CLK_PERIOD_B, units='ns').start())

    dut._log.info('Setup the initial state of signals')
    dut.ENA <= 1 # Always enable
    dut.ENB <= 1 # Always enable
    dut.WEA <= 0

    dut.ADDRA <= 0
    dut.ADDRB <= 0

    dut.DIA <= 0
    dut.DOB <= 0

    yield RisingEdge(dut.CLKA)
    yield RisingEdge(dut.CLKA)

    dut._log.info('Writing all addresses with random integer data')
    dut.ADDRA 	<= 0x00000
	for i in range(dut.ADDRA):
		dut.WEA 	<= 1
		dut.ADDRA 	<= dut.ADDRA + 1 
		dut.DIA 	<= random.randint(0, 1024)
		yield RisingEdge(dut.CLKA)
		dut.WEA 	<= 0
  
    dut._log.info('Reading all memory addresses')
    dut.ADDRB 	<= 0x00000
	for i in range(dut.ADDRB):		
		yield RisingEdge(dut.CLKB)
		yield ReadOnly()
        dut.ADDRB 	<= dut.ADDRB + 1 
        
        # It's a synch read memory, so you need 2 cycles to read the real data
        yield RisingEdge(dut.CLKB)
        yield RisingEdge(dut.CLKB)

        if (dut.DOB.value.integer != dut.DIA.value.integer):
            raise TestFailure("Data read in address " + str(dut.ADDRB) + " is not correct")
    
    raise TestSuccess("Test is OK")