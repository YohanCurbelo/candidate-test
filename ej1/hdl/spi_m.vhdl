library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;
use     ieee.math_real.all;

entity spi_m is
    generic (
        DATA_WIDTH  :   positive    :=  32;
        ADDR_WIDTH  :   positive    :=  4;
        CLK_FREQ    :   positive    :=  100000000;
        SPI_FREQ    :   positive    :=  1000000    
    );
    port (
        clk     :   in  std_logic;
        rst     :   in  std_logic;
        i_start :   in  std_logic;  
        o_int   :   out std_logic;
        -- Datos hacia los registros de configuracion
        o_wdata :   out std_logic_vector(DATA_WIDTH-1 downto 0);
        o_waddr :   out std_logic_vector(ADDR_WIDTH-1 downto 0);
        o_wena  :   out std_logic;
        -- Datos desde los registros de configuracion
        i_rdata :   in  std_logic_vector(DATA_WIDTH-1 downto 0);
        o_raddr :   out std_logic_vector(ADDR_WIDTH-1 downto 0);
        o_rena  :   out std_logic;
        -- SPI interface
        i_MISO  :   in  std_logic;
        o_MOSI  :   out std_logic;   
        o_SCLK  :   out std_logic;
        o_CSn   :   out std_logic
    );
end spi_m;

architecture behavioral of spi_m is

    -- Registro de RX del core
    constant    REG_TX  :   integer :=  0;  -- Registro de RX 0x00
    constant    REG_RX  :   integer :=  4;  -- Registro de RX 0x04

    -- Subtype para el divisor de frecuencia y aprovechar el uso del atributo HIGH
    subtype integer_subtype is integer range 0 to integer(ceil(real((CLK_FREQ/SPI_FREQ)/2)))-1;
    signal div_freq     :   integer_subtype;

    -- Señales intermedias
    signal sclk         :   std_logic;
    signal sclk_en      :   std_logic;
    signal data_tx      :   std_logic_vector(DATA_WIDTH-1 downto 0);
    signal data_rx      :   std_logic_vector(DATA_WIDTH-1 downto 0);
    signal bit_ctr      :   integer range 0 to DATA_WIDTH;              -- Contador para la escritura/lectura bit a bit del dato

    -- Estados de la maquina de estado
    type states is (idle, ask_REG_TX, get_REG_TX, transaction);
    signal state        :   states;

begin

    -- Divisor de frecuencia para obtener SCLK (1 MHz) a partir del AXI_CLK (100 MHz)
    spi_sclk    :   process(clk)
    begin
        if rising_edge(clk) then
            if rst = '0' then
                div_freq    <=  0;
                sclk        <=  '0';
            elsif sclk_en = '1' then
                if div_freq = integer_subtype'HIGH then                    
                    div_freq    <=  0;
                    sclk        <=  not sclk;
                else
                    div_freq    <=  div_freq + 1;
                end if;
            else
                div_freq    <=  0;
                sclk        <=  '0';
            end if;
        end if;
    end process;
    
    o_SCLK  <=  sclk;    

    -- Maquina de estados
    fsm_p   :   process(clk)
    begin
        if rising_edge(clk) then
            if rst = '0' then
                state           <=  idle;
                bit_ctr         <=  0;
                sclk_en         <=  '0';  
                o_CSn           <=  '1';
                o_MOSI          <=  'Z'; 
                o_int           <=  '0';     
                o_wena          <=  '0';              
                o_wdata         <=  (others => '0');
                o_waddr         <=  (others => '0');     
                o_rena          <=  '0';              
                o_raddr         <=  (others => '0');                                       
                data_tx         <=  (others => '0');
            else
                case state is
                    when idle           =>  o_int           <=  '0';  
                                            o_wena          <=  '0';              
                                            o_wdata         <=  (others => '0');
                                            o_waddr         <=  (others => '0');    
                                            if i_start = '1' then
                                                state       <=  ask_REG_TX;                                                
                                                o_raddr     <=  std_logic_vector(to_unsigned(REG_TX, ADDR_WIDTH));
                                                o_rena      <=  '1';                                               
                                            end if;
                                            
                    when ask_REG_TX     =>  state           <=  get_REG_TX;
                                            o_raddr         <=  (others => '0');  
                                            o_rena          <=  '0';                                              
                                            
                    when get_REG_TX     =>  state           <=  transaction;
                                            data_tx         <=  i_rdata;                                     
                                            sclk_en         <=  '1'; 
                                            o_CSn           <=  '0';                         
                    
                    when transaction    =>  if bit_ctr <= DATA_WIDTH then                                                
                                                -- Escritura en MOSI
                                                if div_freq = 0 and sclk = '0' then
                                                    bit_ctr <=  bit_ctr + 1;
                                                    o_MOSI  <=  data_tx(DATA_WIDTH-1);
                                                    data_tx <=  data_tx(DATA_WIDTH-2 downto 0) & '0';
                                                end if ;  
                                                -- Lectura de MISO
                                                if div_freq = 0 and sclk = '1' then
                                                    data_rx <=  data_rx(DATA_WIDTH-2 downto 0) & i_MISO; 
                                                end if;
                                            else
                                                state       <=  idle;
                                                bit_ctr     <=  0;
                                                sclk_en     <=  '0';
                                                o_CSn       <=  '1';
                                                o_MOSI      <=  'Z';
                                                o_int       <=  '1';
                                                o_wena      <=  '1';              
                                                o_wdata     <=  data_rx;
                                                o_waddr     <=  std_logic_vector(to_unsigned(REG_RX, ADDR_WIDTH));                                               
                                            end if;
                    end case;
            end if;
        end if;
    end process;    

end behavioral;