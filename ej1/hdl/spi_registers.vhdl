library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;

entity spi_registers is
    generic (
        DATA_WIDTH  :   positive    :=  32;
        ADDR_WIDTH  :   positive    :=  4
    );
    port (
        clk     :   in  std_logic;
        rst     :   in  std_logic;
        -- Puerto de escritura 1
        i_wdata1:   in  std_logic_vector(DATA_WIDTH-1 downto 0);
        i_waddr1:   in  std_logic_vector(ADDR_WIDTH-1 downto 0);
        i_wena1 :   in  std_logic;
        -- Puerto de escritura 2
        i_wdata2:   in  std_logic_vector(DATA_WIDTH-1 downto 0);
        i_waddr2:   in  std_logic_vector(ADDR_WIDTH-1 downto 0);
        i_wena2 :   in  std_logic;
        -- Puerto de lectura 1
        o_rdata1 :   out std_logic_vector(DATA_WIDTH-1 downto 0);
        i_raddr1 :   in  std_logic_vector(ADDR_WIDTH-1 downto 0);
        i_rena1  :   in  std_logic;        
        -- Puerto de lectura 2
        o_rdata2 :   out std_logic_vector(DATA_WIDTH-1 downto 0);
        i_raddr2 :   in  std_logic_vector(ADDR_WIDTH-1 downto 0);
        i_rena2  :   in  std_logic;       
        -- Trigger del spi
        o_start :   out std_logic
    );
end spi_registers;

architecture behavioral of spi_registers is

    -- Registros de configuracion (0x00 - Dato que se va a trasmitir; 0x04 - Ultimo dato recibido)
    type configuration_register is array (0 to 2**ADDR_WIDTH-1) of std_logic_vector(DATA_WIDTH-1 downto 0);
    signal registers_spi     :   configuration_register;

begin

    -- Acceso a los registros de configuracion
    acceso_reg  :   process(clk)
    begin
        if rising_edge(clk) then
            if rst = '0' then
                o_start         <=  '0';
                o_rdata1        <=  (others => '0');
                o_rdata2        <=  (others => '0');
                registers_spi   <=  (others => (others => '0'));                
            else
                o_start     <=  i_wena1;
                -- Puerto d escritura 1
                if i_wena1 ='1' then                    
                    registers_spi(to_integer(unsigned(i_waddr1))) <=  i_wdata1;                      
                end if;

                -- Puerto de escritura 2
                if i_wena2 ='1' then
                    registers_spi(to_integer(unsigned(i_waddr2))) <=  i_wdata2;  
                end if;

                -- Puerto de lectura 1
                if i_rena1 ='1' then
                    o_rdata1     <=  registers_spi(to_integer(unsigned(i_raddr1)));
                end if;
                
                -- Puerto de lectura 2
                if i_rena2 ='1' then
                    o_rdata2     <=  registers_spi(to_integer(unsigned(i_raddr2)));
                end if;                
            end if;
        end if;
    end process;

end behavioral;