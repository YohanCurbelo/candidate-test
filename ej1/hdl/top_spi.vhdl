library ieee;
use     ieee.std_logic_1164.all;

entity top_spi is
    generic (
        DATA_WIDTH  :   positive    :=  32;
        ADDR_WIDTH  :   positive    :=  4;
        CLK_FREQ    :   positive    :=  100000000;
        SPI_FREQ    :   positive    :=  1000000    
    );
    port (
        clk         :   in std_logic;
        rst         :   in std_logic;
        -- Puerto de escritura
        i_wdata     :   in  std_logic_vector(DATA_WIDTH-1 downto 0);
        i_waddr     :   in  std_logic_vector(ADDR_WIDTH-1 downto 0);
        i_wena      :   in  std_logic;
        -- Puerto de lectura
        o_rdata     :   out std_logic_vector(DATA_WIDTH-1 downto 0);
        i_raddr     :   in  std_logic_vector(ADDR_WIDTH-1 downto 0);
        i_rena      :   in  std_logic;          
        -- Trigger del spi
        o_int       :   out std_logic;
        -- SPI interface
        i_MISO      :   in  std_logic;
        o_MOSI      :   out std_logic;   
        o_SCLK      :   out std_logic;
        o_CSn       :   out std_logic
    );
end top_spi;

architecture behavioral of top_spi is

    -- Señales internas
    signal spi_trigger  :   std_logic;
    signal reg0_data    :   std_logic_vector(DATA_WIDTH-1 downto 0);
    signal reg0_addr    :   std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal reg0_en      :   std_logic;
    signal reg4_data    :   std_logic_vector(DATA_WIDTH-1 downto 0);
    signal reg4_addr    :   std_logic_vector(ADDR_WIDTH-1 downto 0);
    signal reg4_en      :   std_logic;

begin

    REGISTROS   :   entity work.spi_registers
        generic map(
            DATA_WIDTH  =>  DATA_WIDTH,
            ADDR_WIDTH  =>  ADDR_WIDTH             
        )
        port map(
            clk         =>  clk,
            rst         =>  rst,
            -- Puerto de escritura 1 - desde el AXI
            i_wdata1    =>  i_wdata,
            i_waddr1    =>  i_waddr,
            i_wena1     =>  i_wena,
            -- Puerto de lectura 1 - desde el AXI
            o_rdata1    =>  o_rdata,
            i_raddr1    =>  i_raddr,
            i_rena1     =>  i_rena,
            -- Puerto de escritura 2 - desde el SPI
            i_wdata2    =>  reg4_data,
            i_waddr2    =>  reg4_addr,
            i_wena2     =>  reg4_en,            
            -- Puerto de lectura 2 - desde el SPI
            o_rdata2    =>  reg0_data,
            i_raddr2    =>  reg0_addr,
            i_rena2     =>  reg0_en,
            -- Trigger del spi
            o_start     =>  spi_trigger
        );

    SPI : entity work.spi_m
        generic map (
            DATA_WIDTH  =>  DATA_WIDTH,
            ADDR_WIDTH  =>  ADDR_WIDTH,
            CLK_FREQ    =>  CLK_FREQ,
            SPI_FREQ    =>  SPI_FREQ   
        )
        port map (
            clk         =>  clk,
            rst         =>  rst,
            -- Trigger del spi
            i_start     =>  spi_trigger,
            -- IRQ a la CPU
            o_int       =>  o_int,
            -- Puerto de escritura 2
            o_wdata     =>  reg4_data,
            o_waddr     =>  reg4_addr,
            o_wena      =>  reg4_en,
            -- Puerto de lectura 2
            i_rdata     =>  reg0_data,
            o_raddr     =>  reg0_addr,
            o_rena      =>  reg0_en,
            -- SPI interface
            i_MISO      =>  i_MISO,
            o_MOSI      =>  o_MOSI,
            o_SCLK      =>  o_SCLK,
            o_CSn       =>  o_CSn
        );

end behavioral;